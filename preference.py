import json

from PyQt5 import QtCore, QtGui, QtWidgets
from images import Images
from sshtunnel import *
import psycopg2
import pickle
import struct
from message import Message

class Ui_Preference(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(591, 384)
        self.visibleIcon = QtGui.QIcon(Images.get_icon_by_name("visible"))
        self.hiddenIcon = QtGui.QIcon(Images.get_icon_by_name("hidden"))
        self.password_shown = False
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 571, 331))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")
        self.groupBox_server = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_server.setGeometry(QtCore.QRect(10, 20, 271, 91))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_server.setFont(font)
        self.groupBox_server.setObjectName("groupBox_server")
        self.label_host = QtWidgets.QLabel(self.groupBox_server)
        self.label_host.setGeometry(QtCore.QRect(30, 20, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_host.setFont(font)
        self.label_host.setObjectName("label_host")
        self.host = QtWidgets.QLineEdit(self.groupBox_server)
        self.host.setGeometry(QtCore.QRect(80, 20, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.host.setFont(font)
        self.host.setObjectName("host")
        self.label_port = QtWidgets.QLabel(self.groupBox_server)
        self.label_port.setGeometry(QtCore.QRect(30, 50, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_port.setFont(font)
        self.label_port.setObjectName("label_port")
        self.port = QtWidgets.QSpinBox(self.groupBox_server)
        self.port.setGeometry(QtCore.QRect(80, 50, 60, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.port.setFont(font)
        self.port.setObjectName("port")
        self.port.setRange(0, 9999)
        self.check_connect_server = QtWidgets.QPushButton(self.groupBox_server)
        self.check_connect_server.setGeometry(QtCore.QRect(150, 50, 101, 23))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.check_connect_server.setFont(font)
        self.check_connect_server.setObjectName("check_connect_server")
        self.groupBox_db = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_db.setGeometry(QtCore.QRect(10, 110, 271, 211))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_db.setFont(font)
        self.groupBox_db.setObjectName("groupBox_db")
        self.label_host_db = QtWidgets.QLabel(self.groupBox_db)
        self.label_host_db.setGeometry(QtCore.QRect(30, 20, 31, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_host_db.setFont(font)
        self.label_host_db.setObjectName("label_host_db")
        self.host_db = QtWidgets.QLineEdit(self.groupBox_db)
        self.host_db.setGeometry(QtCore.QRect(80, 20, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.host_db.setFont(font)
        self.host_db.setObjectName("host_db")
        self.login_db = QtWidgets.QLineEdit(self.groupBox_db)
        self.login_db.setGeometry(QtCore.QRect(80, 80, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.login_db.setFont(font)
        self.login_db.setObjectName("login_db")
        self.label_login_db = QtWidgets.QLabel(self.groupBox_db)
        self.label_login_db.setGeometry(QtCore.QRect(30, 80, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_login_db.setFont(font)
        self.label_login_db.setObjectName("label_login_db")
        self.port_db = QtWidgets.QSpinBox(self.groupBox_db)
        self.port_db.setGeometry(QtCore.QRect(80, 50, 60, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.port_db.setFont(font)
        self.port_db.setObjectName("port_db")
        self.port_db.setRange(0, 9999)
        self.label_port_db = QtWidgets.QLabel(self.groupBox_db)
        self.label_port_db.setGeometry(QtCore.QRect(30, 50, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_port_db.setFont(font)
        self.label_port_db.setObjectName("label_port_db")
        self.label_password_db = QtWidgets.QLabel(self.groupBox_db)
        self.label_password_db.setGeometry(QtCore.QRect(20, 110, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_password_db.setFont(font)
        self.label_password_db.setObjectName("label_password_db")
        self.password_db = QtWidgets.QLineEdit(self.groupBox_db)
        self.password_db.setGeometry(QtCore.QRect(80, 110, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.password_db.setFont(font)
        self.password_db.setObjectName("password_db")
        self.name_db = QtWidgets.QLineEdit(self.groupBox_db)
        self.name_db.setGeometry(QtCore.QRect(80, 140, 171, 20))
        self.password_db.setEchoMode(QtWidgets.QLineEdit.Password)
        togglepassword_password_db = self.password_db.addAction(self.hiddenIcon, QtWidgets.QLineEdit.TrailingPosition)
        togglepassword_password_db.triggered.connect(lambda: self.on_toggle_password_Action(self.password_db, togglepassword_password_db))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.name_db.setFont(font)
        self.name_db.setObjectName("name_db")

        self.label_name_db = QtWidgets.QLabel(self.groupBox_db)
        self.label_name_db.setGeometry(QtCore.QRect(36, 140, 31, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_name_db.setFont(font)
        self.label_name_db.setObjectName("label_name_db")
        self.check_connect_db = QtWidgets.QPushButton(self.groupBox_db)
        self.check_connect_db.setGeometry(QtCore.QRect(150, 170, 101, 23))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.check_connect_db.setFont(font)
        self.check_connect_db.setObjectName("check_connect_db")
        self.groupBox_ssh = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_ssh.setGeometry(QtCore.QRect(290, 20, 271, 181))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_ssh.setFont(font)
        self.groupBox_ssh.setObjectName("groupBox_ssh")
        self.label_host_ssh = QtWidgets.QLabel(self.groupBox_ssh)
        self.label_host_ssh.setGeometry(QtCore.QRect(10, 20, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_host_ssh.setFont(font)
        self.label_host_ssh.setObjectName("label_host_ssh")
        self.host_ssh = QtWidgets.QLineEdit(self.groupBox_ssh)
        self.host_ssh.setGeometry(QtCore.QRect(80, 20, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.host_ssh.setFont(font)
        self.host_ssh.setObjectName("host_ssh")
        self.login_ssh = QtWidgets.QLineEdit(self.groupBox_ssh)
        self.login_ssh.setGeometry(QtCore.QRect(80, 80, 171, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.login_ssh.setFont(font)
        self.login_ssh.setObjectName("login_ssh")
        self.label_login_ssh = QtWidgets.QLabel(self.groupBox_ssh)
        self.label_login_ssh.setGeometry(QtCore.QRect(30, 80, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_login_ssh.setFont(font)
        self.label_login_ssh.setObjectName("label_login_ssh")
        self.port_ssh = QtWidgets.QSpinBox(self.groupBox_ssh)
        self.port_ssh.setGeometry(QtCore.QRect(80, 50, 60, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.port_ssh.setFont(font)
        self.port_ssh.setObjectName("port_ssh")
        self.port_ssh.setRange(0, 9999)
        self.label_port_ssh = QtWidgets.QLabel(self.groupBox_ssh)
        self.label_port_ssh.setGeometry(QtCore.QRect(30, 50, 40, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_port_ssh.setFont(font)
        self.label_port_ssh.setObjectName("label_port_ssh")
        self.label_password_ssh = QtWidgets.QLabel(self.groupBox_ssh)
        self.label_password_ssh.setGeometry(QtCore.QRect(20, 110, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.label_password_ssh.setFont(font)
        self.label_password_ssh.setObjectName("label_password_ssh")
        self.password_ssh = QtWidgets.QLineEdit(self.groupBox_ssh)
        self.password_ssh.setGeometry(QtCore.QRect(80, 110, 171, 20))
        self.password_ssh.setEchoMode(QtWidgets.QLineEdit.Password)
        togglepassword_password_ssh = self.password_ssh.addAction(self.hiddenIcon, QtWidgets.QLineEdit.TrailingPosition)
        togglepassword_password_ssh.triggered.connect(lambda: self.on_toggle_password_Action(self.password_ssh, togglepassword_password_ssh))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.password_ssh.setFont(font)
        self.password_ssh.setObjectName("password_ssh")
        self.check_connect_ssh = QtWidgets.QPushButton(self.groupBox_ssh)
        self.check_connect_ssh.setGeometry(QtCore.QRect(150, 140, 101, 23))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.check_connect_ssh.setFont(font)
        self.check_connect_ssh.setObjectName("check_connect_ssh")
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(410, 350, 156, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).setText("Відміна")
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Save).setText("Зберегти")
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox.setTitle(_translate("Dialog", "  Налаштування  "))
        self.groupBox_server.setTitle(_translate("Dialog", "З\'єднання"))
        self.label_host.setText(_translate("Dialog", "Хост:"))
        self.label_port.setText(_translate("Dialog", "Порт:"))
        self.check_connect_server.setText(_translate("Dialog", "Перевірка"))
        self.groupBox_db.setTitle(_translate("Dialog", " База данних "))
        self.label_host_db.setText(_translate("Dialog", "Хост:"))
        self.label_login_db.setText(_translate("Dialog", "Логін:"))
        self.label_port_db.setText(_translate("Dialog", "Порт:"))
        self.label_password_db.setText(_translate("Dialog", "Пароль:"))
        self.label_name_db.setText(_translate("Dialog", "База:"))
        self.check_connect_db.setText(_translate("Dialog", "Перевірка"))
        self.groupBox_ssh.setTitle(_translate("Dialog", " SSH тунель "))
        self.label_host_ssh.setText(_translate("Dialog", "Сервер:"))
        self.label_login_ssh.setText(_translate("Dialog", "Логін:"))
        self.label_port_ssh.setText(_translate("Dialog", "Порт:"))
        self.label_password_ssh.setText(_translate("Dialog", "Пароль:"))
        self.check_connect_ssh.setText(_translate("Dialog", "Перевірка"))


    def on_toggle_password_Action(self, lineEditPass, togglepasswordAction):
        if not self.password_shown:
            lineEditPass.setEchoMode(QtWidgets.QLineEdit.Normal)
            self.password_shown = True
            togglepasswordAction.setIcon(self.visibleIcon)
        else:
            lineEditPass.setEchoMode(QtWidgets.QLineEdit.Password)
            self.password_shown = False
            togglepasswordAction.setIcon(self.hiddenIcon)


    def check(self):

        try:
            server = open_tunnel(
                (self.host_ssh.text(), self.port_ssh.value()),
                ssh_username=self.login_ssh.text(),
                ssh_password=self.password_ssh.text(),
                remote_bind_address=(self.host.text(), self.port.value()))
            server.start()
            if server.tunnel_is_up:
                conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                conn.connect((self.host.text(), server.local_bind_port))
                result = {"check_connect": True}
                message_to_send = json.dumps(result).encode()

                #message_to_send = "bye".encode("UTF-8")
                conn.send(len(message_to_send).to_bytes(2, byteorder='big'))
                conn.send(message_to_send)

                length = int.from_bytes(conn.recv(2), byteorder='big')
                received = conn.recv(length).decode("UTF-8")
                data = json.loads(received.encode())


                if data["status"]:
                    Message.message_info("З'єднання встановленно.")

        except Exception as e:
            Message.message_error("Помилка!!!", e.__str__())
        finally:
            server.stop()

    def check_ssh(self):
        try:
            server = open_tunnel(
                (self.host_ssh.text(), self.port_ssh.value()),
                ssh_username=self.login_ssh.text(),
                ssh_password=self.password_ssh.text(),
                remote_bind_address=(self.host.text(), self.port.value()))
            server.start()
            if server.tunnel_is_up:
                Message.message_info("З'єднання встановленно.")
        except Exception as e:
            Message.message_error("Помилка!!!", e.__str__())
        finally:
            server.stop()

    def check_db(self):
        try:
            server = open_tunnel(
                (self.host_ssh.text(), self.port_ssh.value()),
                ssh_username=self.login_ssh.text(),
                ssh_password=self.password_ssh.text(),
                remote_bind_address=(self.host_db.text(), self.port_db.value()))
            server.start()
            if server.tunnel_is_up:
                try:
                    self.conn = psycopg2.connect(
                        database=self.name_db.text(),
                        user=self.login_db.text(),
                        password=self.password_db.text(),
                        host=self.host_db.text(),
                        port=server.local_bind_port)
                    self.cursor = self.conn.cursor()
                    self.cursor.close()
                    Message.message_info("З'єднання встановленно.")
                except Exception as e:
                    Message.message_error("Помилка!!!", e.__str__())
        except Exception as e:
            Message.message_error("Помилка!!!", e.__str__())
        finally:
            server.stop()


