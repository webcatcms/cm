from PyQt5 import QtCore, QtGui, QtWidgets
from images import Images
from model import Model


class List(QtWidgets.QTreeWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setDragDropMode(self.DragDrop)
        self.setSelectionMode(self.ExtendedSelection)
        self.setAcceptDrops(True)
        self.setRootIsDecorated(False)
        self.setHeaderLabels(["", "Назва"])
        self.setIconSize(QtCore.QSize(20, 20))
        self.setColumnWidth(0, 21)
        self.setColumnWidth(1, 100)
        self.setHeaderHidden(True)


    def dropEvent(self, event):
        if event.source() == self:
            event.setDropAction(QtCore.Qt.MoveAction)
            super().dropEvent(event)
            item = self.itemAt(event.pos())
            index = self.indexFromItem(item)
            print(index.row())




class Ui_ListFile(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(606, 457)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(430, 400, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 221, 441))
        self.groupBox.setObjectName("groupBox")

        layout = QtWidgets.QHBoxLayout(self.groupBox)
        self.list_files = List()
        layout.addWidget(self.list_files)
        layout.setGeometry(QtCore.QRect(10, 20, 201, 411))
        layout.setObjectName("list_file")
        # self.treeView = QtWidgets.QTreeView(self.groupBox)
        # self.treeView.setGeometry(QtCore.QRect(10, 20, 201, 411))
        # self.treeView.setObjectName("treeView")



        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(240, 10, 351, 61))
        self.groupBox_2.setObjectName("groupBox_2")
        self.date_start = QtWidgets.QDateTimeEdit(self.groupBox_2)
        self.date_start.setGeometry(QtCore.QRect(70, 20, 111, 22))
        self.date_start.setObjectName("date_start")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(10, 20, 61, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setGeometry(QtCore.QRect(192, 20, 21, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.date_stop = QtWidgets.QDateTimeEdit(self.groupBox_2)
        self.date_stop.setGeometry(QtCore.QRect(220, 20, 111, 22))
        self.date_stop.setObjectName("date_stop")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox.setTitle(_translate("Dialog", " Список файлів "))
        self.groupBox_2.setTitle(_translate("Dialog", " Властивості "))
        self.label.setText(_translate("Dialog", "Період з:"))
        self.label_2.setText(_translate("Dialog", "По:"))


    def set_list_files(self,id):
        model = Model()
        items = model.get_files(id)

        for i in items:
            item = QtWidgets.QTreeWidgetItem(self.list_files, )
            item.setFlags(item.flags() & ~QtCore.Qt.ItemIsDropEnabled)
            item.setIcon(0, Images.get_icon_by_name("file"))
            item.setData(1, 0, i[0])