from PyQt5.QtCore import QObject, QThread, pyqtSignal
from model import Model
from sshtunnel import *
import pickle
import struct


class Worker(QObject):
    finished = pyqtSignal(str)
    progress = pyqtSignal(int)
    file = None
    items = None

    def run(self):
        self.model = Model()
        self.settings = self.model.get_settings()
        self.BUFFER_SIZE = 4096
        password = self.model.cipher_suite.decrypt(bytes(self.settings[6])).decode("utf-8")
        server = open_tunnel(
            (self.settings[3], self.settings[4]),
            ssh_username=self.settings[5],
            ssh_password=password,
            remote_bind_address=(self.settings[1], self.settings[2]))
        server.start()

        self.client_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_conn.connect((self.settings[1], server.local_bind_port))

        while True:
            if os.path.isfile(self.file):

                file_name = self.file.split('/')[-1]
                file_size = os.path.getsize(self.file)

                data_to_server = {'tablet_id': self.items, 'file_name': file_name, 'file_size': file_size}
                serialized_data = pickle.dumps(data_to_server)

                try:
                    self.client_conn.sendall(struct.pack('>I', len(serialized_data)))
                    self.client_conn.sendall(serialized_data)

                    with open(self.file, "rb") as f:
                        size = 0
                        while True:
                            bytes_read = f.read(self.BUFFER_SIZE)
                            if not bytes_read:

                                break

                            self.client_conn.sendall(bytes_read)
                            size += len(bytes_read)
                            self.progress.emit(size)

                    try:
                        data_size = struct.unpack('>I', self.client_conn.recv(4))[0]
                        received_payload = b""
                        reamining_payload_size = data_size
                        while reamining_payload_size != 0:
                            received_payload += self.client_conn.recv(reamining_payload_size)
                            reamining_payload_size = data_size - len(received_payload)
                        data = pickle.loads(received_payload)
                        self.finished.emit(data['message'])

                    except Exception as e:
                        pass
                    self.client_conn.close()
                    break

                except Exception as e:
                    pass
        server.stop()
    def get_file(self, file):
        self.file = file

    def get_items(self, items):
        self.items = items


