from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from model import Model
from images import Images
from sshtunnel import *
import json
import cv2
import datetime

class Worker(QObject):
    finished = pyqtSignal(str)
    progress = pyqtSignal(int)
    file = None

    def run(self):
        self.model = Model()
        self.settings = self.model.get_settings()
        self.BUFFER_SIZE = 4096
        password = self.model.cipher_suite.decrypt(bytes(self.settings[6])).decode("utf-8")
        server = open_tunnel(
            (self.settings[3], self.settings[4]),
            ssh_username=self.settings[5],
            ssh_password=password,
            remote_bind_address=(self.settings[1], self.settings[2]))
        server.start()

        self.client_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_conn.connect((self.settings[1], server.local_bind_port))

        while True:
            if os.path.isfile(self.file):

                file_name = self.file.split('/')[-1]
                file_size = os.path.getsize(self.file)
                file_duraction = data = cv2.VideoCapture(self.file)
                frames = data.get(cv2.CAP_PROP_FRAME_COUNT)
                fps = data.get(cv2.CAP_PROP_FPS)
                seconds = round(frames / fps)
                video_time = datetime.timedelta(seconds=seconds)
                data_to_server = {'file_name': file_name, 'file_size': file_size, 'file_duraction': str(video_time)}
                d = json.dumps(data_to_server).encode()



                try:
                    self.client_conn.send(len(d).to_bytes(2, byteorder='big'))
                    self.client_conn.send(d)

                    with open(self.file, "rb") as f:
                        size = 0
                        while True:
                            bytes_read = f.read(self.BUFFER_SIZE)
                            if not bytes_read:
                                break

                            self.client_conn.sendall(bytes_read)
                            size += len(bytes_read)
                            self.progress.emit(size)

                    try:
                        length = int.from_bytes(self.client_conn.recv(2), byteorder='big')
                        received = self.client_conn.recv(length).decode("UTF-8")
                        data = json.loads(received.encode())
                        self.finished.emit(data['message'])

                    except Exception as e:
                        pass
                    self.client_conn.close()
                    break

                except Exception as e:
                    pass
        server.stop()
        server.close()

    def get_file(self, file):
        self.file = file


class FileDownload(QtWidgets.QWidget):
    request_work = pyqtSignal(object)
    #thread = QThread()
    #worker = Worker()
    model = Model()
    settings = model.get_settings()
    file = None
    BUFFER_SIZE = 4096


    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(384, 153)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(10, 110, 361, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.progressBar = QtWidgets.QProgressBar(Dialog)
        self.progressBar.setGeometry(QtCore.QRect(10, 70, 362, 23))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.progressBar.setFont(font)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.progressBar.setHidden(True)
        self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
        self.file_name = QtWidgets.QLabel(Dialog)
        self.file_name.setGeometry(QtCore.QRect(180, 20, 191, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.file_name.setFont(font)
        self.file_name.setText("")
        self.file_name.setObjectName("file_name")
        self.download_file = QtWidgets.QPushButton(Dialog)
        self.download_file.setGeometry(QtCore.QRect(10, 20, 101, 23))
        self.download_file.setObjectName("download_file")
        self.download_file.setDisabled(True)
        self.download_file.clicked.connect(self.download)
        self.browse_file = QtWidgets.QPushButton(Dialog)
        self.browse_file.setGeometry(QtCore.QRect(120, 20, 51, 23))
        self.browse_file.setObjectName("browse_file")
        self.browse_file.clicked.connect(self.open_file)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.close)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.download_file.setText(_translate("Dialog", "Завантажити"))
        self.browse_file.setText(_translate("Dialog", "..."))


    def open_file(self):
        self.worker = Worker()
        self.request_work.connect(self.worker.get_file)
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        self.file, _ = QtWidgets.QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Video Files (*.mp4)", options=options)
        if os.path.isfile(self.file):
            self.request_work.emit(self.file)
            self.download_file.setDisabled(False)
            self.file_name.setText(self.file.split('/')[-1])


    def file_download(self):
        dialog = QtWidgets.QDialog()
        self.setupUi(dialog)
        dialog.setWindowTitle("Завантажити")
        dialog.setWindowIcon(Images.get_icon_by_name("icon"))
        dialog.exec_()


    def download(self):
        try:
            self.thread = QThread()
            self.worker.moveToThread(self.thread)
            self.thread.started.connect(self.worker.run)
            self.worker.progress.connect(self.progress)
            self.worker.finished.connect(self.finished)
            self.worker.finished.connect(self.thread.quit)
            self.worker.finished.connect(self.worker.deleteLater)
            self.thread.finished.connect(self.thread.deleteLater)
            self.thread.start()
        except:
            pass



        self.download_file.setDisabled(True)
        self.browse_file.setDisabled(True)
        self.progressBar.setHidden(False)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setDisabled(True)
        self.progressBar.setRange(0, os.path.getsize(self.file))

    def progress(self, v):
        self.progressBar.setValue(v)

    def finished(self, v):
        self.progressBar.setFormat(v)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setDisabled(False)