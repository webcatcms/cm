import model
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal
from model import Model
from images import Images
from message import Message
from datetime import *


class List(QtWidgets.QTreeWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        # self.setDragDropMode(self.DragDrop)
        # self.setSelectionMode(self.ExtendedSelection)
        # self.setAcceptDrops(True)
        # self.setRootIsDecorated(False)
        # self.setHeaderLabels(["", "Назва",""])
        # self.setIconSize(QtCore.QSize(20, 20))
        # self.setColumnWidth(0, 21)
        # self.setColumnWidth(1, 100)
        # self.setHeaderHidden(True)
        # self.hideColumn(2)


    def dropEvent(self, event):
        if event.source() == self:
            event.setDropAction(QtCore.Qt.MoveAction)
            super().dropEvent(event)


class PlayList(QtWidgets.QWidget):
    tablet_id = pyqtSignal(str)
    list_id = pyqtSignal(str)
    model = Model()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setFixedSize(550, 554)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(350, 510, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.setObjectName("buttonBox")
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).setText("Відміна")
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Save).setText("Зберегти")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 60, 221, 441))
        self.groupBox.setObjectName("groupBox")
        self.list_files = QtWidgets.QTreeWidget(self.groupBox)
        self.list_files.setGeometry(QtCore.QRect(10, 20, 201, 411))
        self.list_files.setObjectName("list_files")
        self.list_files.setRootIsDecorated(False)
        self.list_files.setHeaderLabels(["", "Назва","3","4"])
        self.list_files.setIconSize(QtCore.QSize(20, 20))
        self.list_files.setColumnWidth(0, 21)
        self.list_files.setColumnWidth(1, 100)
        self.list_files.setColumnWidth(2, 20)
        self.list_files.setHeaderHidden(True)
        self.list_files.hideColumn(3)
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 10, 531, 51))
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.date_start = QtWidgets.QDateEdit(self.groupBox_2)
        self.date_start.setGeometry(QtCore.QRect(260, 20, 111, 22))
        self.date_start.setObjectName("date_start")
        self.date_start.setProperty("showGroupSeparator", False)
        self.date_start.setCalendarPopup(True)
        self.date_start.calendarWidget().setLocale(QtCore.QLocale(QtCore.QLocale.Ukrainian))
        self.date_start.setDate(datetime.now().date())
        self.name = QtWidgets.QLineEdit(self.groupBox_2)
        self.name.setGeometry(QtCore.QRect(55, 20, 140, 21))
        self.name.setObjectName("label_date")
        self.label_name = QtWidgets.QLabel(self.groupBox_2)
        self.label_name.setGeometry(QtCore.QRect(10, 20, 61, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label_name.setFont(font)
        self.label_name.setObjectName("label_date")
        self.label_date = QtWidgets.QLabel(self.groupBox_2)
        self.label_date.setGeometry(QtCore.QRect(200, 20, 61, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label_date.setFont(font)
        self.label_date.setObjectName("label_date")
        self.label_date2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_date2.setGeometry(QtCore.QRect(385, 20, 21, 21))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.label_date2.setFont(font)
        self.label_date2.setObjectName("label_date2")
        self.date_stop = QtWidgets.QDateEdit(self.groupBox_2)
        self.date_stop.setGeometry(QtCore.QRect(410, 20, 111, 22))
        self.date_stop.setObjectName("date_stop")
        self.date_stop.setProperty("showGroupSeparator", False)
        self.date_stop.setCalendarPopup(True)
        self.date_stop.calendarWidget().setLocale(QtCore.QLocale(QtCore.QLocale.Ukrainian))
        self.date_stop.setDate(datetime.now().date())
        self.groupBox_3 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_3.setGeometry(QtCore.QRect(320, 60, 221, 441))
        self.groupBox_3.setObjectName("groupBox_3")
        self.playlist = List(self.groupBox_3)
        self.playlist.setGeometry(QtCore.QRect(10, 20, 201, 411))
        self.playlist.setObjectName("playlist")
        self.playlist.setIconSize(QtCore.QSize(20, 20))
        self.playlist.setHeaderLabels(["", "Назва", "3","4"])
        self.playlist.setColumnWidth(0, 21)
        self.playlist.setColumnWidth(1, 100)
        self.playlist.setColumnWidth(2, 20)
        self.playlist.setRootIsDecorated(False)
        self.playlist.hideColumn(3)
        self.playlist.setHeaderHidden(True)
        self.add_file = QtWidgets.QPushButton(Dialog)
        self.add_file.setGeometry(QtCore.QRect(240, 150, 75, 23))
        self.add_file.clicked.connect(self.set_item_to_playlist)
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        self.add_file.setFont(font)
        self.add_file.setObjectName("add_file")
        self.remove_file = QtWidgets.QPushButton(Dialog)
        self.remove_file.setGeometry(QtCore.QRect(240, 180, 75, 23))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.remove_file.setFont(font)
        self.remove_file.setObjectName("remove_file")
        self.remove_file.clicked.connect(self.remove_item_playlist)
        self.tablet_id.connect(self.set_list_files)
        self.list_id.connect(self.set_file_to_list)
        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(lambda: self.save_playlist(Dialog)) # type: ignore
        self.buttonBox.rejected.connect(Dialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox.setTitle(_translate("Dialog", " Список файлів "))
        self.label_name.setText(_translate("Dialog", "Назва:"))
        self.label_date.setText(_translate("Dialog", "Період з:"))
        self.label_date2.setText(_translate("Dialog", "По:"))
        self.groupBox_3.setTitle(_translate("Dialog", " Новий плейлист "))
        self.add_file.setText(_translate("Dialog", "->"))
        self.remove_file.setText(_translate("Dialog", "<-"))
        # self.groupBox_4.setTitle(_translate("Dialog", "Список плейлистів "))


    def set_list_files(self, id):
        self.name.setProperty("id", id)
        files = self.model.get_files(id)


        for i in files:
            item = QtWidgets.QTreeWidgetItem(self.list_files,)
            item.setIcon(0, Images.get_icon_by_name("file"))
            item.setData(1, 0, i[1])
            item.setData(2, 0, i[2])
            item.setData(3,0,i[0])


    def set_file_to_list(self, id):
        try:
            #self.name.setProperty("id", id)
            plalist = self.model.get_playlist(id)
            self.name.setText(plalist[0][2])
            self.set_list_files(plalist[0][1])
            date_start = datetime.strptime(plalist[0][3], "%d.%m.%Y")
            date_stop = datetime.strptime(plalist[0][4], "%d.%m.%Y")
            self.date_start.setDate(QtCore.QDate(date_start.year, date_start.month, date_start.day))
            self.date_stop.setDate(QtCore.QDate(date_stop.year, date_stop.month, date_stop.day))
            for i in plalist[1]:

                item = QtWidgets.QTreeWidgetItem(self.playlist, )
                item.setIcon(0, Images.get_icon_by_name("file"))
                item.setData(1, 0, i[1])
                item.setData(2, 0, i[2])
                item.setData(3, 0, i[0])
        except:
            pass
    def set_item_to_playlist(self):
        try:

            check = False
            for item in self.playlist.findItems(self.list_files.currentItem().text(3), QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive):

                if self.list_files.currentItem().text(1) == item.text(1):
                    check = True
            if not check:
                i = self.list_files.currentItem().text(1)
                item = QtWidgets.QTreeWidgetItem(self.playlist, )
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsDropEnabled)
                item.setIcon(0, Images.get_icon_by_name("file"))
                item.setData(1, 0, self.list_files.currentItem().text(1))
                item.setData(2, 0, self.list_files.currentItem().text(2))
                item.setData(3, 0, self.list_files.currentItem().text(3))
        except Exception:
            pass

    def remove_item_playlist(self):
        try:
            item = self.playlist.currentItem()
            item.removeChild(item)
            self.playlist.repaint()
        except Exception as e:
            pass


    def save_playlist(self, dialog):
            if not self.name.text() == "":
                data = {}
                data.update({'name': self.name.text(), 'list_id': self.name.property("list_id"), 'tablet_id': self.name.property('id'), 'date_st': self.date_start.text(), 'date_sp': self.date_stop.text()})

                files = []
                for i in range(self.playlist.topLevelItemCount()):
                    item = self.playlist.topLevelItem(i)
                    files.append([item.text(3),item.text(1)])

                data.update({"files": files})

                self.model.set_playlist(data)
                dialog.close()
            else:
                Message.message_info("Назва порожня!!")

