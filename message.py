import sys
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QSize, Qt
from images import Images
class Message():

    @staticmethod
    def message_info(text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        msg.setWindowTitle("Повідомлення")
        msg.setWindowIcon(Images.get_icon_by_name("icon"))
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    @staticmethod
    def message_error(text, detail):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(text)
        msg.setDetailedText(detail)
        msg.setWindowTitle("Повідомлення")
        msg.setWindowIcon(Images.get_icon_by_name("icon"))
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()




