from PyQt5.QtCore import pyqtSignal
from PyQt5 import QtCore, QtGui, QtWidgets
from images import Images
from model import Model
from playlist import PlayList

class Playlists(QtWidgets.QTreeWidget):
    tablet_id = pyqtSignal(dict)
    model = Model()
    def setupUi(self):
        self.setRootIsDecorated(False)
        self.setHeaderLabels(["", "Назва", ""])
        self.setIconSize(QtCore.QSize(20, 20))
        self.setColumnWidth(0, 21)
        self.setColumnWidth(1, 100)
        self.setHeaderHidden(True)
        self.hideColumn(2)

        self.playlists = QtWidgets.QTreeWidget()
        self.playlists.setGeometry(0, 10, 500, 530)
        self.playlists.setObjectName("playlists")
        self.playlists.setRootIsDecorated(False)
        self.playlists.setHeaderLabels(["", "Назва", ""])
        self.playlists.setIconSize(QtCore.QSize(20, 20))
        self.playlists.setColumnWidth(0, 21)
        self.playlists.setColumnWidth(1, 100)
        self.playlists.setHeaderHidden(True)
        self.playlists.hideColumn(2)
        self.playlists.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.playlists.customContextMenuRequested.connect(self.context_menu)
        self.tablet_id.connect(self.set_play_lists)


    def context_menu(self, pos):
        menu = QtWidgets.QMenu(self.playlists)
        add = QtWidgets.QAction("Додати", menu)
        add.setIcon(Images.get_icon_by_name('add'))
        add.triggered.connect(self.add)
        edit = QtWidgets.QAction("Редагувати", menu)
        edit.setIcon(Images.get_icon_by_name('edit'))
        edit.triggered.connect(self.edit)
        delete = QtWidgets.QAction("Видалити", menu)
        delete.setIcon(Images.get_icon_by_name('trash'))
        delete.triggered.connect(self.delete)
        #delete.triggered.connect(self.get_list_tablets)

        # files = QtWidgets.QAction("Плейлисти", menu)
        # files.setIcon(Images.get_icon_by_name('files'))
        # files.triggered.connect(self.get_playlists)


        menu.addAction(add)
        menu.addAction(edit)
        menu.addAction(delete)
        # menu.addAction(files)

        menu.exec_(self.playlists.mapToGlobal(pos))
    #
    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox_4.setTitle(_translate("Dialog", "Список плейлистів "))
    #
    #
    # def add(self):
    #     dialog = QtWidgets.QDialog()
    #     form = PlayList()
    #     form.setupUi(dialog)
    #
    #     form.tablet_id.emit(self.property("data")["id"])
    #
    #     dialog.setWindowTitle('Додати список')
    #     dialog.setWindowIcon(Images.get_icon_by_name("icon"))
    #     #form.buttonBox.accepted.connect(dialog.close)
    #     form.buttonBox.accepted.connect(lambda: self.set_play_lists(self.property("data")))
    #     # form.buttonBox.accepted.connect(self.model.get_playlists(self.property("data")["id"]))
    #     # form.buttonBox.rejected.connect(dialog.close)
    #     dialog.exec_()
    #
    #
    # def edit(self):
    #
    #     dialog = QtWidgets.QDialog()
    #     form = PlayList()
    #     form.setupUi(dialog)
    #     list_id = self.playlists.currentItem().text(2)
    #     form.name.setProperty("list_id", list_id)
    #     form.list_id.emit(list_id)
    #
    #     dialog.setWindowTitle('Додати список')
    #     dialog.setWindowIcon(Images.get_icon_by_name("icon"))
    #     # form.buttonBox.accepted.connect(dialog.close)
    #     form.buttonBox.accepted.connect(lambda: self.set_play_lists(self.property("data")))
    #     # form.buttonBox.accepted.connect(self.model.get_playlists(self.property("data")["id"]))
    #     # form.buttonBox.rejected.connect(dialog.close)
    #     dialog.exec_()
    #
    # def delete(self):
    #     self.model.delete_list(self.playlists.currentItem().text(2))
    #     self.set_play_lists(self.property("data"))
    #
    def set_play_lists(self, data):
        self.setProperty("data", data)
        playlists = self.model.get_playlists(data["id"])
        self.playlists.clear()
    #
    #     for i in playlists:
    #         item = QtWidgets.QTreeWidgetItem(self.playlists,)
    #         item.setIcon(0, Images.get_icon_by_name("file"))
    #         item.setData(1, 0, i[2])
    #         item.setData(2,0,i[0])
