import socket
import pickle
import struct
import os

class Agent():

    def __init__(self):
        self.isAlive = True

        self.client_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.BUFFER_SIZE = 4096
    def main(self):

        try:
            self.client_conn.connect(("localhost", 8888))

            while self.isAlive:

                data_to_server = {'name_id': 'testid'}

                serialized_data = pickle.dumps(data_to_server)
                try:
                    self.client_conn.sendall(struct.pack('>I', len(serialized_data)))
                    self.client_conn.sendall(serialized_data)
                    try:
                        data_size = struct.unpack('>I', self.client_conn.recv(4))[0]
                        received_payload = b""
                        reamining_payload_size = data_size
                        while reamining_payload_size != 0:
                            received_payload += self.client_conn.recv(reamining_payload_size)
                            reamining_payload_size = data_size - len(received_payload)
                        self.last_record = pickle.loads(received_payload)
                    except Exception:
                        pass
                    if self.last_record['file_name']:
                        file_name = "media\\" + self.last_record['file_name']
                        file_size = self.last_record['file_size']

                        with open(file_name, "wb") as f:
                            while True:
                                bytes_read = self.client_conn.recv(self.BUFFER_SIZE)
                                if not bytes_read:
                                    break

                                f.write(bytes_read)

                            self.client_conn.close()


                except Exception:
                    pass

        except:
            pass

        self.client_conn.close()

if __name__ == '__main__':
    a = Agent()
    a.main()