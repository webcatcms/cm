#!/usr/bin/python3
import configparser
import time
import os
import socket
from s_model import Model
import json
import threading
import struct
import pickle



class ContentServer():

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        self.model = Model(self.config)
        self.conn = socket.socket()
        self.conn.bind(("", int(self.config['server']['port'])))
        self.conn.listen()
        self.BUFFER_SIZE = 4096
        self.is_alive = True
        self.lock = threading.Lock()


    def multi_threaded_client(self, connection, lock, is_alive):
        id = 0
        status = False
        while is_alive:

            try:
                length = int.from_bytes(connection.recv(2), byteorder='big')
                received = connection.recv(length).decode("UTF-8")
                data = json.loads(received.encode())
                if 'id' in data:
                    id = data['id']
                    self.lock.acquire()
                    self.model.set_status(id, True)
                    self.lock.release()
            except Exception as e:
                self.lock.acquire()
                self.model.set_status(id, False)
                self.lock.release()
                break


            if not data:
                break

            if 'get_playlists' in data:
                self.lock.acquire()
                playlists = self.model.get_playlists(id)
                self.lock.release()

                for playlist in playlists:
                    try:
                        file = "media/" + playlist[4]
                        file_size = os.path.getsize(file)
                        row = json.dumps(
                            {"playlist_id": playlist[0], "playlist_name": playlist[1], "date_st": playlist[2],
                             "date_sp": playlist[3], "file_name": playlist[4], "file_size": file_size}).encode()

                        connection.send(len(row).to_bytes(2, byteorder='big'))
                        connection.send(row)

                        length = int.from_bytes(connection.recv(2), byteorder='big')
                        received_payload = connection.recv(length).decode("UTF-8")
                    except Exception:
                        continue

                    answer = json.loads(received_payload.encode())
                    if answer['file_exists']:
                        continue
                    else:
                        try:

                            with open(file, "rb") as f:
                                while True:
                                    bytes_read = f.read(self.BUFFER_SIZE)
                                    if not bytes_read:
                                        break

                                    connection.send(bytes_read)

                        except Exception as e:
                            pass


                d = json.dumps({"done": True}).encode()

                try:
                    connection.send(len(d).to_bytes(2, byteorder='big'))
                    connection.send(d)
                except ConnectionResetError:
                    pass

            if 'get_uid' in data:
                try:
                    self.lock.acquire()
                    uid = self.model.get_uid(id)
                    self.lock.release()
                    d = json.dumps({"uid": uid}).encode()
                    connection.send(len(d).to_bytes(2, byteorder='big'))
                    connection.send(d)
                except ConnectionResetError:
                    pass

            try:
                if 'file_name' in data:
                    if not os.path.exists("media"):
                        os.makedirs("media")
                    path_file = "media/" + data['file_name']
                    with open(path_file, "wb") as f:
                        size = 0
                        while True:
                            bytes_read = connection.recv(self.BUFFER_SIZE)
                            if not bytes_read:
                                break
                            f.write(bytes_read)
                            size += len(bytes_read)
                            if size == data['file_size']:
                                break

                    if os.path.getsize(path_file) == data['file_size']:
                        self.lock.acquire()
                        self.model.set_files(data)
                        self.lock.release()
                        message = {"message": "Файл завантажено"}
                        d1 = json.dumps(message).encode()
                        connection.send(len(d1).to_bytes(2, byteorder='big'))
                        connection.send(d1)
                    connection.close()
                    break
            except Exception as e:
                pass

            if 'check_connect' in data:
                result = {"status": True}
                d2 = json.dumps(result).encode()
                connection.send(len(d2).to_bytes(2, byteorder='big'))
                connection.send(d2)


    def main(self):
        while True:
            try:
                Client, address = self.conn.accept()
                Client.settimeout(10)
                accept = threading.Thread(target=self.multi_threaded_client, args=(Client, self.lock, True))
                accept.start()


            except Exception as e:
                pass


        self.conn.close()

if __name__ == '__main__':
    server = ContentServer()
    server.main()
