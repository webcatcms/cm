from PyQt5 import QtWidgets, QtCore, QtGui, Qt
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from model import Model
from images import Images
from time import sleep


class Worker(QObject):
    progress = pyqtSignal(list)
    file = None

    def run(self):
        self.model = Model()

        while True:
            sleep(10)
            try:
                tablet = self.model.get_tablets()
                self.progress.emit(tablet)
            except:
                pass
        server.stop()

    def get_file(self, file):
        self.file = file


class ListTablet(QtWidgets.QWidget):
    thread = QThread()
    worker = Worker()

    def __init__(self):
        super().__init__()

        self.model = Model()
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.progress.connect(self.progress)
        # self.worker.finished.connect(self.finished)
        # self.worker.finished.connect(self.thread.quit)
        # self.worker.finished.connect(self.worker.deleteLater)
        # self.thread.finished.connect(self.thread.deleteLater)
        self.thread.start()

    def setupUi(self):
        self.setGeometry(0, 10, 260, 530)
        self.view = QtWidgets.QTreeWidget()
        self.view.setIconSize(QtCore.QSize(25, 25))
        self.view.setHeaderLabels(["", "Назва", "Вибрати", ""])
        self.view.setRootIsDecorated(False)
        self.view.setHeaderHidden(True)
        self.view.setColumnWidth(0, 25)
        self.view.setColumnWidth(1, 140)
        self.view.setColumnWidth(2, 61)
        self.view.setColumnWidth(3, 1)
        self.view.hideColumn(3)

        window_layout = QtWidgets.QVBoxLayout()
        window_layout.addWidget(self.view)
        self.setLayout(window_layout)

        self.set_list_tablets()


    def doubleClick(self, ev):
        self.view.itemDoubleClicked.connect(ev)

    def Click(self, ev):
        self.view.itemClicked.connect(ev)

    def get_list_tablets(self):
        self.set_list_tablets()

    def set_checkBox(self, item, id):
        widget = QtWidgets.QWidget()
        widget.setObjectName('checkBoxItem')
        lay_out = QtWidgets.QHBoxLayout(widget)
        lay_out.setAlignment(QtCore.Qt.AlignCenter)
        lay_out.setContentsMargins(0, 5, 0, 5)
        checkBox = QtWidgets.QCheckBox(widget)
        checkBox.setProperty('id', id)
        lay_out.addWidget(checkBox)
        self.view.setItemWidget(item, 2, widget)

    def selected_items(self):
        selected = []
        items = self.view.findChildren(QtWidgets.QCheckBox)
        for i in items:
            if i.checkState() == QtCore.Qt.Checked:
                item = int(i.property('id'))
                selected.append(item)
        return selected

    def set_list_tablets(self):

        items = self.model.get_tablets()
        self.view.clear()
        for i in items:
            item = QtWidgets.QTreeWidgetItem(self.view,)
            item.setIcon(0, Images.get_icon_by_name("online") if i[3] else Images.get_icon_by_name("offline"))
            item.setData(1, 0, i[2])

            item.setData(2, 0, i[0])

            #self.set_checkBox(item, i[0])

    def progress(self, items):
        self.view.clear()
        for i in items:
            item = QtWidgets.QTreeWidgetItem(self.view, )
            item.setIcon(0, Images.get_icon_by_name("online") if i[3] else Images.get_icon_by_name("offline"))
            item.setData(1, 0, i[2])
            item.setData(2, 0, i[0])
            #self.set_checkBox(item, i[0])