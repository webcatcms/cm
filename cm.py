import datetime
import os

import model
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from model import Model
from sshtunnel import *
import pickle
import struct
from time import sleep
import subprocess
from main_windows import Ui_MainWindow
from form import Ui_Dialog
from preference import Ui_Preference
from images import Images
from list_tablet import ListTablet
from playlist import PlayList
from tablets import Tablet

class ContentManager(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):

        super(ContentManager, self).__init__(*args, **kwargs)
        self.model = Model()
        self.settings = self.model.get_settings()
        self.ui = Ui_MainWindow()
        self.list = ListTablet()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("Контент менеджер")
        self.setWindowIcon(Images.get_icon_by_name("icon"))
        self.ui.setupUi(self)
        self.list.setupUi()
        self.list.setParent(self.ui.groupBox)
        self.list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.list.customContextMenuRequested.connect(self.context_menu)
        self.ui.playlists.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.playlists.customContextMenuRequested.connect(self.context_menu_playlist)
        # self.list.view.selectionModel().selectionChanged.connect(self.set_hidden)
        self.list.view.clicked.connect(self.get_playlists_tablet)
        self.ui.button_add_tablet.clicked.connect(self.add_tablet)
        self.ui.button_edit_tablet.clicked.connect(self.edit_tablet)
        self.ui.button_remove_tablet.clicked.connect(self.remove_tablet)
        self.ui.button_playlists.clicked.connect(self.get_playlists)

        self.ui.button_preferences.clicked.connect(self.preference)


        self.show()

    def set_hidden(self):
        self.ui.GBC.setHidden(True)

    def context_menu(self, pos):
        self.menu = QtWidgets.QMenu(self.list)
        self.add = QtWidgets.QAction("Додати", self.menu)
        self.add.setIcon(Images.get_icon_by_name('add'))
        self.add.triggered.connect(self.add_tablet)
        self.edit = QtWidgets.QAction("Редагувати", self.menu)
        self.edit.setIcon(Images.get_icon_by_name('edit'))
        self.edit.triggered.connect(self.edit_tablet)
        self.delete = QtWidgets.QAction("Видалити", self.menu)
        self.delete.setIcon(Images.get_icon_by_name('trash'))
        self.delete.triggered.connect(self.remove_tablet)
        self.delete.triggered.connect(self.get_list_tablets)
        self.action_playlists = QtWidgets.QAction("Плейлисти", self.menu)
        self.action_playlists.setIcon(Images.get_icon_by_name('files'))
        self.action_playlists.triggered.connect(self.get_playlists_tablet)

        self.menu.addAction(self.add)
        self.menu.addAction(self.edit)
        self.menu.addAction(self.delete)
        self.menu.addAction(self.action_playlists)
        self.menu.exec_(self.list.mapToGlobal(pos))

    def context_menu_playlist(self, pos):
        self.menu_pl = QtWidgets.QMenu(self.ui.playlists)
        add = QtWidgets.QAction("Додати", self.menu_pl)
        add.setIcon(Images.get_icon_by_name('add'))
        add.triggered.connect(self.add_play_list)
        edit = QtWidgets.QAction("Редагувати", self.menu_pl)
        edit.setIcon(Images.get_icon_by_name('edit'))
        edit.triggered.connect(self.edit_playlist)
        delete = QtWidgets.QAction("Видалити", self.menu_pl)
        delete.setIcon(Images.get_icon_by_name('trash'))
        delete.triggered.connect(self.remove_playlist)
        add_tablet = QtWidgets.QAction("Додати планшет", self.menu_pl)
        add_tablet.triggered.connect(self.add_tablet_to_playlist)

        self.menu_pl.addAction(add)
        self.menu_pl.addAction(edit)
        self.menu_pl.addAction(delete)
        self.menu_pl.addAction(add_tablet)
        self.menu_pl.exec_(self.ui.playlists.mapToGlobal(pos))

    def preference(self):
        dialog = QtWidgets.QDialog()
        form = Ui_Preference()
        form.setupUi(dialog)
        form.buttonBox.accepted.connect(lambda: self.model.set_settings(form.__dict__, dialog))
        form.buttonBox.rejected.connect(dialog.close)
        form.check_connect_server.clicked.connect(form.check)
        form.check_connect_ssh.clicked.connect(form.check_ssh)
        form.check_connect_db.clicked.connect(form.check_db)
        settings = self.model.get_settings()
        form.host.setText(settings[1])
        form.port.setValue(settings[2])
        form.host_ssh.setText(settings[3])
        form.port_ssh.setValue(settings[4])
        form.login_ssh.setText(settings[5])
        form.host_db.setText(settings[7])
        form.login_db.setText(settings[8])
        form.name_db.setText(settings[10])
        form.port_db.setValue(settings[11])
        password_ssh = self.model.cipher_suite.decrypt(bytes(settings[6]))
        form.password_ssh.setText(password_ssh.decode("utf-8"))
        password_db = self.model.cipher_suite.decrypt(bytes(settings[9]))
        form.password_db.setText(password_db.decode("utf-8"))
        dialog.setWindowTitle("Налаштування")
        dialog.setWindowIcon(Images.get_icon_by_name("preference"))
        dialog.exec_()

    def add_tablet(self):
        dialog = QtWidgets.QDialog()
        form = Ui_Dialog()
        form.setupUi(dialog)
        dialog.setWindowTitle("Додати")
        dialog.setWindowIcon(Images.get_icon_by_name("icon"))
        form.buttonBox.accepted.connect(lambda: self.model.save_tablet(form.__dict__, dialog, True))
        form.buttonBox.accepted.connect(self.get_list_tablets)
        form.buttonBox.rejected.connect(dialog.close)
        dialog.exec_()

    def edit_tablet(self):
        try:
            dialog = QtWidgets.QDialog()
            form = Ui_Dialog()
            form.setupUi(dialog)
            tablet = self.model.get_tablet_by_id(self.list.view.currentItem().text(2))
            form.tablet_name.setText(tablet[2])
            form.tablet_id.setText(tablet[1])
            form.tablet_id.setProperty("id", tablet[0])
            dialog.setWindowTitle("Додати")
            dialog.setWindowIcon(Images.get_icon_by_name("icon"))
            form.buttonBox.accepted.connect(lambda: self.model.save_tablet(form.__dict__, dialog, False))
            form.buttonBox.accepted.connect(self.get_list_tablets)
            form.buttonBox.rejected.connect(dialog.close)
            dialog.exec_()
        except Exception:
            pass

    def remove_tablet(self):
        try:
            self.model.delete_tablet(self.list.view.currentItem().text(2))
            self.get_list_tablets()
        except Exception:
            pass

    def get_list_tablets(self):
        # def set_checkBox(item, id):
        #     widget = QtWidgets.QWidget()
        #     widget.setObjectName('checkBoxItem')
        #     lay_out = QtWidgets.QHBoxLayout(widget)
        #     lay_out.setAlignment(QtCore.Qt.AlignCenter)
        #     lay_out.setContentsMargins(0, 5, 0, 5)
        #     checkBox = QtWidgets.QCheckBox(widget)
        #     checkBox.setProperty('id', id)
        #     lay_out.addWidget(checkBox)
        #     self.list.view.setItemWidget(item, 2, widget)

        items = self.model.get_tablets()
        self.list.view.clear()
        for i in items:
            item = QtWidgets.QTreeWidgetItem(self.list.view, )
            item.setIcon(0, Images.get_icon_by_name("online") if i[3] else Images.get_icon_by_name("offline"))
            item.setData(1, 0, i[2])
            item.setData(2, 0, i[0])

            # set_checkBox(item, i[0])


    def get_playlists(self):
        try:
            self.ui.GBC.setTitle("Плейлисти")
            playlists = self.model.get_playlists()
            self.ui.playlists.clear()
            for i in playlists:
                item = QtWidgets.QTreeWidgetItem(self.ui.playlists, )
                item.setIcon(0, Images.get_icon_by_name("file"))
                item.setData(1, 0, i[2])
                item.setData(2, 0, i[0])
            self.ui.GBC.setHidden(False)

        except Exception:
            pass
    def get_playlists_tablet(self):
        try:
            self.ui.GBC.setTitle(self.list.view.currentItem().text(1))
            playlists = self.model.get_playlists_by_id(self.list.view.currentItem().text(2))
            self.ui.playlists.setProperty("tablet_id", self.list.view.currentItem().text(2))
            self.ui.playlists.clear()
            for i in playlists:
                item = QtWidgets.QTreeWidgetItem(self.ui.playlists, )
                item.setIcon(0, Images.get_icon_by_name("file"))
                item.setData(1, 0, i[2])
                item.setData(2, 0, i[0])
            self.ui.GBC.setHidden(False)
            # form.tablet_id.emit({"id": self.list.view.currentItem().text(3), "new": False})
        except:
            pass




        # dialog = QtWidgets.QDialog()
        # form = Playlists()
        # form.setupUi(dialog)
        # self.list.view.currentItem().text(3)
        # # form.tablet_id.emit({"id": self.ui.list.currentItem().text(3), "new": False})
        #
        # # dialog.setWindowTitle(self.ui.list.currentItem().text(1))
        # dialog.setWindowIcon(Images.get_icon_by_name("icon"))
        # # form.buttonBox.accepted.connect(lambda: self.model.save_tablet(form.__dict__, dialog, False))
        # # form.buttonBox.accepted.connect(self.get_list_tablets)
        # # form.buttonBox.rejected.connect(dialog.close)
        # dialog.exec_()



        # self.ui.list.tablet_id.emit({"id": self.ui.list.currentItem().text(3), "new": False})

    def set_play_lists(self):
        try:
            playlists = self.model.get_playlists()
            self.ui.playlists.clear()


            for i in playlists:
                item = QtWidgets.QTreeWidgetItem(self.ui.playlists,)
                item.setIcon(0, Images.get_icon_by_name("file"))
                item.setData(1, 0, i[2])
                item.setData(2,0,i[0])
        except:
            pass
    def add_play_list(self):

        dialog = QtWidgets.QDialog()
        form = PlayList()
        form.setupUi(dialog)
        form.name.setText(f"Playlist-{datetime.datetime.now().date()}")
        form.tablet_id.emit(self.ui.playlists.property("tablet_id"))
        dialog.setWindowTitle('Додати список')
        dialog.setWindowIcon(Images.get_icon_by_name("icon"))
        form.buttonBox.accepted.connect(self.set_play_lists)
        dialog.exec_()

    def edit_playlist(self):

        dialog = QtWidgets.QDialog()
        form = PlayList()
        form.setupUi(dialog)
        list_id = self.ui.playlists.currentItem().text(2)
        form.name.setProperty("list_id", list_id)
        form.list_id.emit(list_id)
        dialog.setWindowTitle('Додати список')
        dialog.setWindowIcon(Images.get_icon_by_name("icon"))
        form.buttonBox.accepted.connect(self.set_play_lists)
        dialog.exec_()

    def remove_playlist(self):
        self.model.delete_list(self.ui.playlists.currentItem().text(2))
        playlists = self.model.get_playlists()
        self.ui.playlists.clear()

        for i in playlists:
            item = QtWidgets.QTreeWidgetItem(self.ui.playlists, )
            item.setIcon(0, Images.get_icon_by_name("file"))
            item.setData(1, 0, i[2])
            item.setData(2, 0, i[0])
        # self.set_play_lists(self.property("data"))

    def show_playlists(self, item):
        pass

    def add_tablet_to_playlist(self):
        try:
            dialog = QtWidgets.QDialog()
            form = Tablet()
            form.setupUi(dialog)
            form.list_id.emit(self.ui.playlists.currentItem().text(2))
            dialog.setWindowTitle('Додати планшет')
            dialog.setWindowIcon(Images.get_icon_by_name("icon"))
            # form.buttonBox.accepted.connect(lambda: self.set_play_lists(self.property("data")))
            dialog.exec_()
        except:
            pass

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ex = ContentManager()
    sys.exit(app.exec_())


