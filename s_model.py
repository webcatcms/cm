import os
from cryptography.fernet import Fernet
import psycopg2
import datetime
import configparser

class Model(object):
    _instance = None

    def __new__(cls, config):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self,config):
        key = b'lN2WR8D7rbAJx9Z2l3qpAtnHKqxWHleMp4M2LuSubPY='

        # password_db = self.cipher_suite.decrypt(bytes(self.settings[9])).decode("utf-8")

        try:
            self.conn = psycopg2.connect(
                database=config['database']['dbname'],
                user=config['database']['username'],
                password=config['database']['password'],
                host=config['database']['hostname'],
                port=config['database']['port'],
                sslmode='require')
            self.cursor = self.conn.cursor()
            print("connect db")
        except Exception as e:
            pass

    def set_files(self, data):

        try:

            self.cursor.execute("INSERT INTO files(name,duraction) VALUES (%s,%s)", (data["file_name"],data["file_duraction"]))
            self.conn.commit()
            print('files write to base')
        except Exception as e:
            pass



    def set_status(self, id, status):
        try:
            self.cursor.execute("UPDATE tablets SET status = %s WHERE id = %s", (status, id))
            self.conn.commit()
        except Exception:
            pass

    def get_playlists(self, id):

        self.cursor.execute("SELECT p.id, p.name,p.date_st,p.date_sp,f.name FROM playlists p LEFT JOIN file_to_list ftl ON (ftl.list_id = p.id) LEFT JOIN files f ON(f.id = ftl.file_id) LEFT JOIN tablet_to_list ttl ON (ttl.list_id = p.id) WHERE ttl.tablet_id = %s;" % (id,))

        return self.cursor.fetchall()

    def get_uid(self, id):
        self.cursor.execute(
            "SELECT uid FROM tablets t WHERE id = %s;" % (
            id,))

        return self.cursor.fetchone()[0]
