from images import Images
from PyQt5 import QtCore, QtGui, QtWidgets
from file_download import FileDownload
class Ui_MainWindow(QtWidgets.QWidget):


    def setupUi(self, MainWindow):
        self.form_download = FileDownload()

        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(550, 620)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.GBC = QtWidgets.QGroupBox(self.centralwidget)
        self.GBC.setGeometry(QtCore.QRect(280, 60, 261, 541))
        self.GBC.setObjectName("groupBoxContent")
        self.GBC.setStyleSheet('QGroupBox:title {'
                      'subcontrol-origin: margin;'
                      'subcontrol-position: top center;'
                      'padding-left: 10px;'
                      'padding-right: 10px; }')
        self.GBC.setHidden(True)
        self.playlists = QtWidgets.QTreeWidget(self.GBC)
        self.playlists.setGeometry(10, 20, 241, 512)
        self.playlists.setObjectName("playlists")
        self.playlists.setRootIsDecorated(False)
        self.playlists.setHeaderLabels(["", "Назва", ""])
        self.playlists.setIconSize(QtCore.QSize(20, 20))
        self.playlists.setColumnWidth(0, 21)
        self.playlists.setColumnWidth(1, 100)
        self.playlists.setHeaderHidden(True)
        self.playlists.hideColumn(2)


        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 60, 261, 541))
        self.groupBox.setObjectName("groupBox")
        MainWindow.setCentralWidget(self.centralwidget)


        self.button_add_tablet = QtWidgets.QPushButton(MainWindow)
        self.button_add_tablet.setGeometry(QtCore.QRect(10, 10, 40, 40))
        self.button_add_tablet.setIcon(Images.get_icon_by_name("add_tablet"))
        self.button_add_tablet.setIconSize(QtCore.QSize(35, 35))
        self.button_add_tablet.setToolTip("Додати планшет")

        self.button_edit_tablet = QtWidgets.QPushButton(MainWindow)
        self.button_edit_tablet.setGeometry(QtCore.QRect(55, 10, 40, 40))
        self.button_edit_tablet.setIcon(Images.get_icon_by_name("edit_tablet"))
        self.button_edit_tablet.setIconSize(QtCore.QSize(35, 35))
        self.button_edit_tablet.setToolTip("Редагувати планшет")

        self.button_remove_tablet = QtWidgets.QPushButton(MainWindow)
        self.button_remove_tablet.setGeometry(QtCore.QRect(100, 10, 40, 40))
        self.button_remove_tablet.setIcon(Images.get_icon_by_name("remove_tablet"))
        self.button_remove_tablet.setIconSize(QtCore.QSize(35, 35))
        self.button_remove_tablet.setToolTip("Видалити планшет")

        self.button_preferences = QtWidgets.QPushButton(MainWindow)
        self.button_preferences.setGeometry(QtCore.QRect(235, 10, 40, 40))
        self.button_preferences.setIcon(Images.get_icon_by_name("preferences"))
        self.button_preferences.setIconSize(QtCore.QSize(35, 35))
        self.button_preferences.setToolTip("Налаштування")

        self.button_playlists = QtWidgets.QPushButton(MainWindow)
        self.button_playlists.setGeometry(QtCore.QRect(190, 10, 40, 40))
        self.button_playlists.setIcon(Images.get_icon_by_name("playlists"))
        self.button_playlists.setIconSize(QtCore.QSize(35, 35))
        self.button_playlists.setToolTip("Плейлисти")

        self.button_file_upload = QtWidgets.QPushButton(MainWindow)
        self.button_file_upload.setGeometry(QtCore.QRect(145, 10, 40, 40))
        self.button_file_upload.setIcon(Images.get_icon_by_name("upload_file"))
        self.button_file_upload.setIconSize(QtCore.QSize(35, 35))
        self.button_file_upload.setToolTip("Завантажити файл")
        self.button_file_upload.clicked.connect(self.form_download.file_download)





        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1012, 21))
        self.menubar.setObjectName("menubar")
        self.menubar.setHidden(True)
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menu2 = QtWidgets.QMenu(self.menubar)
        self.menu2.setObjectName("menu2")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_add = QtWidgets.QAction(MainWindow)
        self.action_add.setObjectName("action_add")
        self.action_add.setIcon(Images.get_icon_by_name("add"))

        self.action_edit = QtWidgets.QAction(MainWindow)
        self.action_edit.setObjectName("action_edit")
        self.action_edit.setIcon(Images.get_icon_by_name("edit"))

        self.action_remove = QtWidgets.QAction(MainWindow)
        self.action_remove.setObjectName("action_remove")
        self.action_remove.setIcon(Images.get_icon_by_name("trash"))

        self.action_download_file = QtWidgets.QAction(MainWindow)
        self.action_download_file.setObjectName("download_file")
        self.action_download_file.setIcon(Images.get_icon_by_name("file_download"))
        self.action_download_file.triggered.connect(self.form_download.file_download)

        self.action_playlists = QtWidgets.QAction(MainWindow)
        self.action_playlists.setObjectName("playlists")
        self.action_playlists.setIcon(Images.get_icon_by_name("files"))




        self.action_preference = QtWidgets.QAction(MainWindow)
        self.action_preference.setObjectName("action_preference")
        #self.action_preference.setIcon(Images.get_icon_by_name("preference"))
        self.action_preference.setText("Налаштування")

        self.menu.addAction(self.action_add)
        self.menu.addAction(self.action_edit)
        self.menu.addAction(self.action_remove)
        self.menu.addAction(self.action_download_file)
        self.menu.addAction(self.action_playlists)
        self.menu2.addAction(self.action_preference)
        # self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.action_preference)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.groupBox.setTitle(_translate("MainWindow", "  Список планшетів  "))
        self.GBC.setTitle(_translate("MainWindow", "  Список плайлистів  "))
        self.menu.setTitle(_translate("MainWindow", "Основні дії"))
        self.menu2.setTitle(_translate("MainWindow", "Налаштування"))
        self.action_add.setText(_translate("MainWindow", "Новий"))
        self.action_edit.setText(_translate("MainWindow", "Редагувати"))
        self.action_remove.setText(_translate("MainWindow", "Видалити"))
        self.action_download_file.setText(_translate("MainWindow", "Завантажити файл"))
        self.action_playlists.setText(_translate("MainWindow", "Плейлисти"))
        self.action_preference.setText(_translate("MainWindow", "Налаштування"))
