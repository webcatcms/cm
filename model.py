import os
import sqlite3
from cryptography.fernet import Fernet
from sshtunnel import *
import psycopg2
from message import Message
from PyQt5 import QtGui, QtWidgets
import datetime
class Model(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        key = b'lN2WR8D7rbAJx9Z2l3qpAtnHKqxWHleMp4M2LuSubPY='
        self.cipher_suite = Fernet(key)
        self.settings_conn = sqlite3.connect(os.getcwd() + "\database.db", check_same_thread=False)
        self.setting_cursor = self.settings_conn.cursor()
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.settings = self.setting_cursor.fetchone()

        password_ssh = self.cipher_suite.decrypt(bytes(self.settings[6])).decode("utf-8")

        password_db = self.cipher_suite.decrypt(bytes(self.settings[9])).decode("utf-8")


        self.server = open_tunnel(
            (self.settings[3], self.settings[4]),
            ssh_username=self.settings[5],
            ssh_password=password_ssh,
            remote_bind_address=(self.settings[7], self.settings[11]))
        self.server.start()

        try:
            self.conn = psycopg2.connect(
                database=self.settings[10],
                user=self.settings[8],
                password=password_db,
                host=self.settings[7],
                port=self.server.local_bind_port,
                sslmode='require')
            self.cursor = self.conn.cursor()
        except Exception as e:
            Message.message_error("Не можливо підключитись до БД!!!", e.__str__())


    def get_settings(self):
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def set_settings(self, data, dialog):
        args = (
            data['host'].text(),
            data['port'].value(),
            data['host_ssh'].text(),
            data['port_ssh'].value(),
            data['login_ssh'].text(),
            self.cipher_suite.encrypt(bytes(str(data['password_ssh'].text().replace(" ", "")), "utf-8")),
            data['host_db'].text(),
            data['login_db'].text(),
            self.cipher_suite.encrypt(bytes(str(data['password_db'].text().replace(" ", "")), "utf-8")),
            data['name_db'].text(),
            data['port_db'].value()

            )

        sql = ''' UPDATE settings SET 
            host=?,
            port=?,
            host_ssh=?,
            port_ssh=?,
            login_ssh=?,
            password_ssh=?,
            host_db=?,
            login_db=?,
            password_db=?,
            name_db=?,
            port_db=?   
            '''

        self.setting_cursor.execute(sql, args)
        self.settings_conn.commit()
        dialog.close()

    def set_log(self, data):
        self.setting_cursor.execute("INSERT INTO logs(timestamp , level, event, data) VALUES(?,?,?,?)", data)
        self.settings_conn.commit()

    def get_log(self):
        self.setting_cursor.execute('SELECT * FROM logs ORDER BY timestamp DESC LIMIT 1000')
        return self.setting_cursor.fetchall()

    def search_log(self, date_in, date_out):
        date_in = date_in.split('.')
        date_out = date_out.split('.')
        date_in = date_in[2] + '-' + date_in[1] + '-' + date_in[0] + r' 00:00:00'
        date_out = date_out[2] + '-' + date_out[1] + '-' + date_out[0] + r' 23:59:59'
        self.setting_cursor.execute('SELECT * FROM logs WHERE timestamp > ? AND timestamp < ?  ORDER BY timestamp  ASC ', (date_in, date_out,))
        return self.setting_cursor.fetchall()

    def delete_log(self, date_in, date_out):
        date_in = date_in.split('.')
        date_out = date_out.split('.')
        date_in = date_in[2] + '-' + date_in[1] + '-' + date_in[0] + r' 00:00:00'
        date_out = date_out[2] + '-' + date_out[1] + '-' + date_out[0] + r' 23:59:59'
        self.setting_cursor.execute('DELETE FROM logs WHERE timestamp > ? AND timestamp < ?', (date_in, date_out,))
        self.settings_conn.commit()

    def range_date(self):
        self.setting_cursor.execute('select min(timestamp) as date_in, max(timestamp) as date_out from logs')
        return self.setting_cursor.fetchone()

    def get_status(self):
        self.setting_cursor.execute('SELECT time_start FROM settings LIMIT 1')
        return self.setting_cursor.fetchone()

    def get_tablets(self):
        try:
            self.cursor.execute(
                "SELECT * FROM tablets ORDER BY name ASC")
            return self.cursor.fetchall()
        except:
            pass
    def get_tfl(self):
        self.cursor.execute(
            "SELECT * FROM tablets ORDER BY name ASC")
        return self.cursor.fetchall()

    def get_tablet_by_id(self, id):
        self.cursor.execute(
            "SELECT * FROM tablets WHERE id = %s" % (id,))
        return self.cursor.fetchone()

    def get_tablets_by_list_id(self, id):
        self.cursor.execute(
            "SELECT * FROM tablets t LEFT JOIN tablet_to_list ttl ON (ttl.tablet_id = t.id) WHERE ttl.list_id = %s;" % (id,))
        return self.cursor.fetchall()

    def set_tablets_to_playlist(self, data):

        self.cursor.execute("DELETE from tablet_to_list WHERE list_id = %s" % (data["list_id"],))

        for tablet in data['tablets']:
            self.cursor.execute("INSERT INTO tablet_to_list ("
                                "tablet_id,"
                                "list_id) VALUES (%s,%s)",
                                (tablet, data["list_id"]))

        self.conn.commit()

    def delete_tablet(self, id):
        self.cursor.execute("DELETE from tablets WHERE id = %s" % (id,))
        self.cursor.execute("SELECT id FROM playlists WHERE tablet_id = %s" % (id,))
        lists_id =self.cursor.fetchall()
        for id in lists_id:
            self.cursor.execute("DELETE from playlists WHERE id = %s" % (id,))
            self.cursor.execute("DELETE from file_to_list WHERE list_id = %s" % (id,))

        self.conn.commit()

    def save_tablet(self, data, dialog, new):
        if new:
            self.cursor.execute("INSERT INTO tablets ("
                                "name, "
                                "uid,"
                                "status) VALUES (%s,%s,%s)",
                                (data["tablet_name"].text(),
                                 data["tablet_id"].text(),
                                 False))

        else:
            self.cursor.execute("UPDATE tablets SET "
                                "name = %s,"
                                "uid = %s WHERE id = %s",
                                (data["tablet_name"].text(),
                                 data["tablet_id"].text(),
                                 str(data["tablet_id"].property('id'))))
        self.conn.commit()

        dialog.close()

    def get_files(self, id=None):
        self.cursor.execute(
            "SELECT * FROM files ORDER BY name")
        return self.cursor.fetchall()

    def get_playlists_by_id(self, id):

        try:
            if id != None:
                self.cursor.execute(
                    "SELECT * FROM playlists p  LEFT JOIN tablet_to_list ttl ON (p.id = ttl.list_id) WHERE ttl.tablet_id = %s; " % (id,))

            return self.cursor.fetchall()
        except:
            pass

    def get_playlists(self):

        self.cursor.execute("SELECT * FROM playlists;")

        return self.cursor.fetchall()

    def get_playlist(self, id):

        self.cursor.execute(
            "SELECT * FROM playlists WHERE id  = %s; " % (id,))
        playlist = self.cursor.fetchone()

        self.cursor.execute(
            "SELECT files.id, name, files.duraction FROM file_to_list ftl LEFT JOIN files  ON (ftl.file_id  = files.id) where ftl.list_id  = %s ORDER BY ftl.sort ; " % (id,))

        files = self.cursor.fetchall()

        return (playlist, files)

    def set_playlist(self, data):

        if not data['list_id'] == None:
            self.cursor.execute("UPDATE playlists SET "
                                "tablet_id = %s, "
                                "name = %s,"
                                "date_st = %s,"
                                "date_sp = %s WHERE id = %s",
                                (data["tablet_id"],
                                 data["name"],
                                 data["date_st"],
                                 data["date_sp"],
                                 data['list_id']))

            self.cursor.execute("DELETE from file_to_list WHERE list_id = %s" % (data["list_id"],))
            count = 1
            for file in data['files']:
                self.cursor.execute("INSERT INTO file_to_list ("
                                    "file_id,"
                                    "list_id,"
                                    "sort) VALUES (%s,%s,%s)",
                                    (file[0], data["list_id"], count))
                count += 1

        else:
            self.cursor.execute("INSERT INTO playlists ("
                                "name,"
                                "date_st,"
                                "date_sp) VALUES (%s,%s,%s) RETURNING id",
                                (data["name"],
                                 data["date_st"],
                                 data["date_sp"]))

            last_record = self.cursor.fetchone()[0]
            for file in data['files']:
                self.cursor.execute("INSERT INTO file_to_list ("
                                    "file_id,"
                                    "list_id) VALUES (%s,%s)",
                                    (file[0],
                                     last_record))



        self.conn.commit()

        return True

    def delete_list(self, id):
        self.cursor.execute("DELETE from playlists WHERE id = %s" % (id,))
        self.cursor.execute("DELETE from file_to_list WHERE list_id = %s" % (id,))
        self.cursor.execute("DELETE from tablet_to_list WHERE list_id = %s" % (id,))
        self.conn.commit()


    def message_info(self, str_text):
        msg = QtWidgets.QMessageBox()
        msg.setWindowIcon(QtGui.QIcon("./img/icon.png"))
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        msg.setWindowTitle("Повідомлення")
        msg.setText(str_text)
        msg.addButton('Закрити', QtWidgets.QMessageBox.RejectRole)
        msg.exec()